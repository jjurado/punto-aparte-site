<div class="container">
    <div class="content-nosotros">
    <div class="titulos">
        <ul>
            <li>
                <a class="active" href="nosotros.php">
                    Nosotros
                </a>
            </li>
            <li>
                <a href="equipo.php">
                    Equipo
                </a>
            </li>
        </ul>
    </div>
    <div class="contenido">
        <p>En el <b>Grupo Editorial .Puntoaparte</b> nos especializamos en <b>biblioinnovación,</b> es decir, en generar experiencias editoriales innovadoras, sobre todo entre públicos no lectores. Partimos de nuestro conocimiento y experiencia en el lenguaje visual y el diseño a la hora de crear dichas experiencias. Creemos que los libros pueden cambiar el mundo y, sobre todo, a un país como Colombia.</p>
    </div>
</div>
</div>
