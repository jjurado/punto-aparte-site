        <footer class="">
            <div class="container">
                <div class="foot-container">
                    <div class="">
                        <small>Bogotá D.C., Colombia</small>
                    </div>
                    <div class="">
                        <small>Copyright &copy; 2016 Puntoaparte</small>
                    </div>
                    <div class="social-icons">
                        <a href="#">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-pinterest" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-behance" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-youtube" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
