<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="https://use.fontawesome.com/ee9f60ab82.js"></script>

    <link rel="stylesheet" href="style.css">

    <title>Puntoaparte</title>

    <!-- [if lt IE 9]
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    [endif] -->

  </head>
  <body>
      <header >
          <div class="container">
              <nav class="navbar navbar-default menu-container" role="navigation">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-1-collapse">
                          <i class="fa fa-bars" aria-hidden="true"></i>
                      </button>
                      <div class="logo">
                          <a class="" href="index.php"><img class="img-responsive" src="imgs/logo.png" alt="LOGO"></a>
                      </div>
                  </div>
                  <div class="collapse navbar-collapse navbar-1-collapse">
                      <ul class="nav navbar-nav navbar-right">
                          <li><a  class="active" href="index.php"><b>PROYECTOS</b></a></li>
                          <li><a href="noticias.php"><b>NOTICIAS</b></a></li>
                          <li><a href="nosotros.php"><b>NOSOTROS</b></a></li>
                          <li><a href="contacto.php"><b>CONTACTO</b></a></li>
                      </ul>
                  </div>
              </nav>
              <div class="search-container">
                  <form class="collapse navbar-form navbar-right pull-right" role="search">
                      <div class="form-cont">
                          <input type="text" class="form-control" placeholder="Buscar">
                          <button type="submit" class="btn btn-default search-btn">Buscar</button>
                      </div>
                  </form>
                  <button class="pull-right buscar" type="button" name="navbar-toggle" data-toggle="collapse" data-target=".navbar-form">
                      <i class="fa fa-search fa-rotate-90" aria-hidden="true"></i>
                  </button>
              </div>
         </div>
      </header>
