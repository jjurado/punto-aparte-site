<div class="container">
    <div class="contacto-container">
    <div class="contacto">
        <div class="contacto-header">
            <h2>Contacto</h2>
        </div>
        <div class="">
            <div class="email">
                <div class="icon">
                    <a href="mailto:contacto@puntoaparte.com.co"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                </div>
                <div class="info-contacto">
                    <a href="mailto:contacto@puntoaparte.com.co">contacto@puntoaparte.com.co</a>
                </div>
            </div>
            <div class="phone">
                <div class="icon">
                    <a href="tel:+5717455952"> <i class="fa fa-phone fa-rotate-270" aria-hidden="true"></i></a>
                </div>
                <div class="info-contacto">
                    <a href="tel:+5717455952">+57 1 745 5952</a>
                </div>
            </div>
            <div class="mapa">
            <div class="icon">
                <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i>
                </a>
            </div>
            <div class="info-contacto">
                <a href="#">Carrera 9 #71-38, oficina 605 Bogotá, Colombia</a>
            </div>
        </div>
        </div>
    </div>
    <div class="google-maps col-xs-12 col-md-9">
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d15929.71061570007!2d-76.5314816!3d3.48794745!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sco!4v1481894738818" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
</div>
