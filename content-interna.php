<div class="container">
    <div class="cerrar">
        <a href="index.php"> <img class="img-responsive" src="imgs/close.png" alt="Cerrar"> </a>
    </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
              <div class="item active">
                  <img src="imgs/imagen-muestra.jpg" class="img-responsive" alt="Carros">
                  <div class="info-cont">
                        <div class="info-item">
                          <div class="info">
                              <div class="titulo">
                                  <h2><b>Título del proyecto,</b></h2>
                              </div>
                              <div class="subtitulo">
                                  <h3>Subtitulo del proyecto</h3>
                              </div>
                          </div>
                          <div class="clientes">
                              <h4>Cliente/s</h4>
                          </div>
                        </div>
                        <div class="links-cont">
                            <div class="links">
                                <a href="#">Ver más <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                                <a href="#">Descargar <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                   </div>
              </div>
            <div class="item">
                <img src="imgs/imagen-muestra.jpg" class="img-responsive" alt="Carros">
                <div class="info-cont">
                    <div class="info-item">
                        <div class="info">
                            <div class="titulo">
                                <h2><b>Título del proyecto,</b></h2>
                            </div>
                            <div class="subtitulo">
                                <h3>Subtitulo</h3>
                            </div>
                        </div>
                        <div class="clientes">
                            <h4>Cliente/s</h4>
                        </div>
                    </div>
                    <div class="">
                          <div class="links-cont">
                              <div class="links">
                                  <a href="#">Ver más <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                              </div>
                              <div class="links">
                                  <a href="#">Descargar <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                              </div>
                          </div>
                     </div>
                 </div>
            </div>
          </div>
        </div>
</div>
